# Donation Form

This module adds simple donation functionality via [Stripe](https://stripe.com).

## Requirements

1. Download Site
2. Composer Install

## Steps to create the module

### "The plan"

1. __Create module__
2. __Create form__ within module
    * __Implement [Stripe.js and Elements](https://stripe.com/docs/stripe-js)__ from Stripe to get CC token to submit w/ form
    * Include a “name” field to capture that data in Drupal
    * On submission, after validation:
        * Check donation dropdown field; if set to “other”, use custom amount field for donation
          * Use Stripe PHP Library to create a charge object and submit
3. __Create entity for Donors__
4. __Create controller for Stripe data mgmt__
5. Adjust form wiring / create new function to __save user data to Drupal__ when a donation is made
6. __Create custom JS behaviors__
    * Validation
    * Helper functions for custom vs standard donation amounts
        * If user adjusts dropdown to a $ amount, clear the custom field
        * If user adjusts custom field, set dropdown to “custom”

### Pain points in the initial setup

I ran into two major issues while working on this module:

1. Local SSL certificate management (Stripe requires https)
2. Drupal ignorance

I spent some time working on the local SSL issue but had to abandon it for time, which left me unable to use Stripe.js effectively, so the form is not currently able to create charges w/ Stripe.

My inexperience with Drupal meant a fairly steep learning curve, and thus far I've not been able to wrap my head completely around the following concerns:
* Remote JS file integration
    * I couldn't quite get the libraries.yml method or the hook_ technique to load stripe.js for my module; a hacky solution I discovered was left out of this repo for cleanliness
* Where to capture user data
    * I decided that creating a donor entity would be appropriate but ran out of time attempting to work through this processed


## Opportunities for improvement

Other than finishing the functionality as specified, there are a few things I would look to improve:

* Place the donation form on every page (in a modal?) instead of a separate page
* Naming the module "donation_form" got weird once I was actually creating the form classes; I'd like to think up a name that doesn't use the word "Form"
* Not building in a theme or tailoring the form weighs heavily on my CSS-loving heart, but I'm not honestly sure whether visual adjustments in a module like this are a best practice in Drupal
