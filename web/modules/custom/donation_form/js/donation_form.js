/**
 * @File
 * Javascript for the Donation Form module.
 */

(function ($, Drupal, drupalSettings, window) {
  'use strict';

  Drupal.behaviors.donationForm = {
    attach: function (context, drupalSettings) {

      // fire up stripe.js
      var stripe = Stripe(drupalSettings.stripe_public_key);
      var elements = stripe.elements();
      var card = elements.create('card');
      card.mount('#card-element');

      // DOM vars
      var form = document.getElementById('donation-form');
      var cardError = document.getElementById('card-errors');
      var $hiddenInput = $('input[name="token"]');
      var $name = $('#edit-name');
      $name.after('<div class="name-error error"></div>');
      var $amountSelect = $('#edit-amount');
      var $amountCustom = $('#edit-custom-amount');
      $('.form-item-custom-amount').after('<div class="amount-error error"></div>');

      /*
      ** event listeners
      */

      // magically display card errors from Stripe
      card.addEventListener('change', function(event) {
        if (event.error) {
          cardError.textContent = event.error.message;
        } else {
          cardError.textContent = '';
        }
      });

      // Create a token or display an error when the form is submitted.
      form.addEventListener('submit', function(event) {
        event.preventDefault();
        validateDonationForm();
      });

      $amountSelect.change(function() {
        if (this.value != "" && this.value != "custom")
        $amountCustom.val("");
      });

      $amountCustom.change(function() {
        if (this.value != '') {
          $amountSelect.children().last().prop('selected',true);
        }
      });

      /*
      ** validation + DOM manipulation funcs
      */

      // runs all validations then submits the form if they return true
      function validateDonationForm() {
        // check both non-promise validations first
        var valid_name = validateName();
        var valid_amt = validateAmount();

        // now check Stripe validation
        if (valid_name && valid_amt) {
          // because we know name & amount are valid, set submit flag to true:
          validateCardSubmit(true);
        } else {
          // no submit flag, this is just to validate
          validateCardSubmit();
        }
      }

      // ask Stripe for a token; if not, display an error
      function validateCardSubmit(submit) {
        var valid = false;
        // first we see if any card data has been entered at all
        // using the magic of Stripe-provided utility classes:
        if ($(".ElementsApp").hasClass('is-empty') == true) {
          cardError.textContent = "Please enter your card information.";
        } else {
          // there is definitely card info; transmit to Stripe
          stripe.createToken(card).then(function(result) {
            if (result.error) {
              // card info isn't ready; magically display Stripe error
              cardError.textContent = result.error.message;
            } else {
              // add token to hidden form input to be saved
              console.log(result.token);
              $hiddenInput.val(result.token.id);
              if (submit) {
                form.submit();
              }
            }
          });
        }
        return valid;
      }

      function validateName() {
        var valid = false;
        if ($name.val() == '') {
          // name is blank
          $(".name-error").text("Please enter your name.");
        } else {
          $(".name-error").text("");
          var valid = true;
        }
        return valid;
      }

      function validateAmount() {
        var valid = false;

        if ($amountCustom.val() != '') {
          // donor entered a custom value
          valid = true;
        } else if ($amountSelect.val() != '') {
          // donor chose a menu value
          valid = true;
        }
        if (!valid) {
          $(".amount-error").text("Please select a donation amount.");
        } else {
          $(".amount-error").text("");
        }
        return valid;
      }

    }
  };
})(jQuery, Drupal, window);
