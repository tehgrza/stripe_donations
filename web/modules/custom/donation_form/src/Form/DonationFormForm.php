<?php

namespace Drupal\donation_form\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\stripe_api\StripeApiService;

class DonationFormForm extends FormBase {

  // public function __construct(StripeApiService $stripe_api) {
  //   $this->stripeApi = $stripe_api;
  // }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
   return 'donation_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['name'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Name'),
    );
    $form['card-element'] = array(
      '#markup' => t('
        <div class="label">Credit card information</div>
        <div id="card-element"></div>
        <div id="card-errors"></div>
      '),
    );
    $form['token'] = array(
      '#type' => 'hidden',
      '#name' => 'token',
      #'#markup' => t('<input type="hidden" id="token" name="token" />'),
    );
    $form['amount'] = [
      '#type' => 'select',
      '#title' => $this->t('Select donation amount'),
      '#options' => [
        '' => '',
        '5' => '$5',
        '25' => '$25',
        '100' => '$100',
        'custom' => 'Custom',
      ],
    ];
    $form['custom_amount'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Custom amount (USD)'),
    );
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (strlen($form_state->getValue('name')) < 1) {
      $form_state->setErrorByName('name', $this->t('Please enter your name.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // foreach ($form_state->getValues() as $key => $value) {
    //   drupal_set_message($key . ': ' . $value);
    // }

    $name = $form_state->getValue('name');
    $token = $form_state->getValue('token');
    $amount = $form_state->getValue('amount');
    $custom_amount = $form_state->getValue('custom_amount');

    if ($custom_amount != '') {
      $amount = $custom_amount;
    }

    $fields = array(
      'name' =>  $name,
      'amount' => $amount,
      'token' => $token,
    );
    db_insert('donations')
      ->fields($fields)
      ->execute();

    // Set your secret key: remember to change this to your live secret key in production
    // See your keys here: https://dashboard.stripe.com/account/apikeys
    $stripe_secret_key = \Drupal::config('donation_config.settings')->get('stripe_secret_key');
    \Stripe\Stripe::setApiKey($stripe_secret_key);

    // Charge the user's card:
    $charge = \Stripe\Charge::create(array(
      "amount" => $amount*100, # Stripe counts in pennies
      "currency" => "usd",
      "description" => "Donation to a Good Cause",
      "capture" => false,
      "source" => $token,
    ));

    drupal_set_message("Thank you!");

  }

}
