<?php

namespace Drupal\donation_form\Form;

use Drupal\Core\Form\ConfigFormBase;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Form\FormStateInterface;

class DonationConfigForm extends ConfigFormBase {
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'donation_config_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'donation_config.settings',
    ];
  }
  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, Request $request = NULL) {

    $config = $this->config('donation_config.settings');
    $form['stripe_public_key'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Stripe Public API Key'),
      '#default_value' => $config->get('stripe_public_key'),
    );
    $form['stripe_secret_key'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Stripe *Secret* API Key'),
      '#default_value' => $config->get('stripe_secret_key'),
    );
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $this->config('donation_config.settings')
      ->set('stripe_public_key', $values['stripe_public_key'])
      ->set('stripe_secret_key', $values['stripe_secret_key'])
      ->save();
    drupal_set_message("Save successful");
  }

}
