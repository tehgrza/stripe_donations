
<?php
/**
 * @file
 * Contains \Drupal\manage_inventory\Entity\Contact.
 */
namespace Drupal\manage_inventory\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\user\UserInterface;

/**
 * Defines the Donations entity.
 *
 * @ingroup donation_form
 *
 * @ContentEntityType(
 *   id = "donations",
 *   label = @Translation("Donations"),
 *   base_table = "donations",
 *   data_table = "donations_field_data",
 *   translatable = TRUE,
 *   admin_permission = "administer donations entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "name" = "name",
 *     "amount" = "amount",
 *     "token" = "token",
 *   },
 * )
 */
