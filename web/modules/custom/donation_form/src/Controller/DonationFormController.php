<?php
/**
 * @file
 * Contains \Drupal\donation_form\Controller\Display.
 */

namespace Drupal\donation_form\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Class Display.
 *
 * @package Drupal\my_custom\Controller
 */
class DonationFormController extends ControllerBase {

  /**
   * showdata.
   *
   * @return string
   *   Return Table format data.
   */
  public function showdata() {

    // you can write your own query to fetch the data I have given my example.

    $result = \Drupal::database()->select('donations', 'n')
            ->fields('n', array('id', 'name', 'amount', 'token'))
            ->execute()->fetchAllAssoc('id');

    // Create the row element.
    $rows = array();
    foreach ($result as $row => $content) {
      $rows[] = array(
        'data' => array($content->id, $content->name, "$".$content->amount.".00", $content->token));
    }

    // Create the header.
    $header = array('id', 'name', 'amount', 'token');
    $output = array(
      '#theme' => 'table',    // Here you can write #type also instead of #theme.
      '#header' => $header,
      '#rows' => $rows
    );
    return $output;
  }
}
